import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class MyWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setupUI()

    def setupUI(self):
        self.setGeometry(800, 200, 1280, 720)
        self.lbl_list = []
        self.le_list = []
        self.VL = []
        self.HL = []
        self.GB = []

        lbl_list_cnt = 10
        lbl_name = []
        for i in range(0, 11):
            lbl_name.append("label"+ str(i))
        for (i, content) in enumerate(lbl_name):
            self.lbl_list.append(QLabel(content, self))
            self.lbl_list[i].setGeometry(20, 20+i*20, 160, 20)
            self.lbl_list[i].setAlignment(Qt.AlignCenter)

        for i in range(0, 11):
            self.le_list.append(QLineEdit("", self))
            self.le_list[i].setGeometry(200, 20+i*20, 160, 20)
            self.le_list[i].setAlignment(Qt.AlignLeft)
            if i==0 or i== 1 or i==2:
                self.le_list[i].setText(lbl_name[i])
                self.le_list[i].setReadOnly(True)

        self.VL.append(QVBoxLayout())
        self.VL.append(QVBoxLayout())
        self.VL.append(QVBoxLayout())
        self.HL.append(QHBoxLayout())
        self.GB.append(QGroupBox("Setting"))
        self.GB[0].setAlignment(Qt.AlignCenter)
        
        for i in range(0, 11):
            self.VL[0].addWidget(self.lbl_list[i],  10, Qt.AlignCenter | Qt.AlignTop)
            self.VL[1].addWidget(self.le_list[i], 10, Qt.AlignCenter | Qt.AlignTop)

        for i in range(0, 2):
            self.HL[0].addLayout(self.VL[i], 10)
        self.GB[0].setLayout(self.HL[0])
        self.VL[2].addWidget(self.GB[0], 10,(Qt.AlignLeft | Qt.AlignHCenter))
        self.VL[2].setGeometry(QRect(20, 20, 340, 300))
        self.setLayout(self.VL[2])

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mywindow = MyWindow()
    mywindow.show()
    app.exec_()